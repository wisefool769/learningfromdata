from __future__ import division
import numpy as np
from math import log, sqrt

def iter_converge(x0, func, delta = 0.0001, max_iter = 100000):
    ret = x0
    x = x0
    for i in range(max_iter):
        xprime = func(x)
        diff = abs(xprime - x)
        if diff < delta:
            ret = xprime
            break
        x = xprime
    return ret

def gf_fact(dvc):
    return lambda N : 2 ** float(N) if dvc == 0 else float(N) ** dvc

def vc(delta, dvc):
    growth_function = gf_fact(dvc)
    return lambda N: sqrt(8/np.float(N) *\
            log((4 * growth_function(N)) / delta))

def rademacher(delta, dvc):
    growth_function = gf_fact(dvc)
    return lambda N: \
            sqrt((2 * log(2 * N * growth_function(N))) / N) +\
            sqrt((2/N) * log(1/delta)) +\
            1 / N


def parrondo(delta, dvc):
    growth_function = gf_fact(dvc)
    def implicit_eps(N):
        def feps(eps):
            return sqrt(1/N * (2 * eps +\
                    log(6 * growth_function(2 * N)/delta)))
        return feps

    def fN(N):
        f = implicit_eps(N)
        return iter_converge(1000, f)

    return fN

def devroye(delta, dvc):
    def log_gf(N):
        ret = 0
        if dvc == 0:
            ret = N * log(2)
        else:
            ret = dvc * log(N)
        return ret
    
    def implicit_eps(N):
        def feps(eps):
            return sqrt(1/(2 * N) * (4 * eps * (1 + eps) + \
                    (log(4) + log_gf(N ** 2) - log(delta))))
        return feps

    def fN(N):
        f = implicit_eps(N)
        return iter_converge(1000,f)

    return fN

