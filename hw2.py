from __future__ import print_function
from math import exp
import random
from ml_lib import Perceptron, Tester, error_cat
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

def hoeffding(N, eps):
    return 2 * exp(-2 * eps ** 2 * N)

#P1
def flip_coins():
    s = sum(random.randint(0,1) for i in range(10))
    return float(s) / 10.0

def flip_1k_coins():
    vs = [flip_coins() for _ in range(1000)]
    results = {}
    results["rand"] = vs[random.randint(0,999)]
    results["first"] = vs[0]
    results["min"]  = min(vs)
    return results

def experiment():
    ntrials = 100000
    agg = {}
    labels = "rand first min".split()
    for label in labels:
        agg[label] = 0.0
    for i in range(ntrials):
        result = flip_1k_coins()
        for label in labels:
            agg[label] += result[label]
        if (i-1) % 1000 == 0:
            print("trial %d: " % i, end ="")
            for label in labels:
                print("%s - %.4f\t" % (label, agg[label]/float(i)), end="")
            print("")
    for label in labels:
            agg[label] /= float(ntrials)
    return agg

def p1():
    result = experiment()
    print(result)

#P2
class P5Trial(object):
    def __init__(self):
        self.perceptron = Perceptron(100, size = 1, plot = False)
        self.D = None
        self.lm = None
        self.N = 100
        self.g = None

    def get_Ein(self):
        pred_in = np.sign(self.lm.predict(self.X))
        return error_cat(pred_in, self.y)

    def get_Eout(self):
        new_points = [self.perceptron.choose_random_point() \
                for i in range(1000)]
        new_data = [[p, self.perceptron.decide(self.g, p)]\
                for p in new_points]
        X, y = self.perceptron.regr_transform(new_data)
        pred_out = np.sign(self.lm.predict(X))
        return error_cat(pred_out, y)
        
    def fit_model(self):
        self.lm = LinearRegression(fit_intercept = False)
        self.lm.fit(self.X, self.y)
        self.lm.coef_ /= self.lm.coef_[2]

    
    def main(self):
        perceptron_result = self.perceptron.main()
        self.g = self.perceptron.g
        self.X, self.y = \
                self.perceptron.regr_transform(self.perceptron.D)
        self.fit_model()
        result = {
                "Ein" : self.get_Ein(),
                "Eout" : self.get_Eout()
                }
        return result

class P8Trial(object):
    def __init__(self):
        self.X = None
        self.y = None
        self.N = 1000

    def generate_data_set(self):
        X = np.random.rand(self.N,2) * 2 - 1
        y = np.array(map(lambda x: np.sign(x[0]**2 + x[1] ** 2 - 0.6), X))
        to_flip = random.sample(range(self.N), int(0.1 * self.N))
        y = np.array([-1 * y[i] if i in to_flip else y[i]\
                for i in range(self.N)])
        return X,y
        
    def plot_points(self):
        colors = {-1 : "red", 1 : "green"}
        plt.scatter(self.X[:,0], self.X[:,1], c = [colors[i] for i in self.y])
        plt.show()
        import ipdb; ipdb.set_trace()

    def fit_model(self):
        self.lm = LinearRegression(fit_intercept = False)
        self.lm.fit(self.X, self.y)

    def get_Ein(self):
        pred_in = np.sign(self.lm.predict(self.X))
        return error_cat(pred_in, self.y)

    def main(self):
        self.X, self.y = self.generate_data_set()
        self.fit_model()
        return {"Ein" : self.get_Ein()}

class P9Trial(P8Trial):
    @staticmethod
    def nonlinear_transform(X):
        x1 = X[:,0]
        x2 = X[:,1]
        N = len(x1)
        return np.column_stack(((np.zeros(N) + 1),\
                x1,x2,x1 * x2, x1 ** 2, x2 ** 2))

    def plot_points(self):
        colors = {-1 : "red", 1 : "green"}
        plt.scatter(self.X[:,1], self.X[:,2], c = [colors[i] for i in self.y])
        plt.show()

    def get_Eout(self):
        X, y = self.generate_data_set()
        X = self.nonlinear_transform(X)
        pred = np.sign(self.lm.predict(X))
        return error_cat(pred, y)

    def main(self):
        self.X, self.y = self.generate_data_set()
        self.X = self.nonlinear_transform(self.X)
        self.fit_model()
        return {"Ein" : self.get_Ein(),
                "Eout" : self.get_Eout(),
                "coef" : self.lm.coef_}

def p5():
    t = Tester(P5Trial, 1000, ["Ein", "Eout"], verbose = False)
    result = t.main()
    return result

def p6():
    p = Perceptron(N = 10, size = 1, plot = False, pocket = True)
    t = Tester(p, 1000, ["n"], verbose = True)
    result = t.main()
    return result

def p8():
    p8t = P8Trial()
    t = Tester(p8t, 1000, ["Ein"], verbose = False)
    res = t.main()
    print(res)

def p9():
    p9t = P9Trial()
    t = Tester(p9t, 10, ["Ein", "coef", "Eout"])
    res = t.main()
    print(res)


def main():
    p9()

    import ipdb; ipdb.set_trace()

if __name__ == "__main__":
    main()
		



