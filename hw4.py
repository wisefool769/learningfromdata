from __future__ import print_function
import matplotlib.pyplot as plt
from math import log
import numpy as np
import generalization_bounds as gb
from sklearn.linear_model import LinearRegression
import random
import scipy.integrate as integrate

def p1():
    nbound = lambda N, epsilon, delta, dvc:\
        (N >= (8/epsilon**2) * log((4*N**dvc)/delta))
    p1_nbound = lambda N : nbound(N, 0.05, 0.05, 10)
    for choice in (4e5,4.2e5,4.4e5,4.6e5,4.8e5):
        if p1_nbound(choice):
            return choice
    return None


def plot_bounds():
    n_range = np.concatenate([np.arange(1,10,1) , np.arange(10, 20000, 10)])
    delta = 0.05
    dvc = 50
    bound_funcs = [("vc" , gb.vc),
            ("rademacher" , gb.rademacher),
            ("parrondo", gb.parrondo),
            ("devroye", gb.devroye)]

    small_n = 5
    large_n = 10000

    small_idx = np.where(n_range == small_n)[0][0]
    large_idx = np.where(n_range == large_n)[0][0]

    for name,bf in bound_funcs:
        bound_func = bf(delta, dvc)
        bounds = np.array([bound_func(n) for n in n_range])
        print("%s : (N = %d) - %.3f, (N = %d) - %.3f" %\
                (name, small_n, bounds[small_idx],
                    large_n,bounds[large_idx]))
        plt.plot(n_range, bounds, lw = 2)




    plt.legend([i[0] for i in bound_funcs], loc="upper right")
    plt.ylim([0,1])

    plt.show()

class BiasVariance(object):
    def __init__(self):
        self.target = lambda x: np.sin(np.pi * x)
        self.N =2
        self.ntrial = 10000

    def generate_data(self):
        def get_data_point():
            pt = random.uniform(-1,1)
            return pt

        X = np.array([get_data_point() for _ in range(self.N)])
        y = self.target(X)
        return (X.reshape(-1,1),y)


class LinearBiasVariance(BiasVariance):
    def __init__(self):
        super(LinearBiasVariance, self).__init__()

    @staticmethod
    def fit(D):
        lm = LinearRegression(fit_intercept = False)
        lm.fit(D[0], D[1])
        return lm.coef_

    def trial(self):
        D = self.generate_data()
        result = {}
        coef = self.fit(D)
        result["coef"] = coef
        result["sqcoef"] = coef ** 2
        return result

    def calc_bias(self, beta):
        return integrate.quad(lambda x: (beta * x  - self.target(x)) ** 2,
                              -1, 1)[0]

    def calc_variance(self, moment2):
        return integrate.quad(lambda x: moment2 * x ** 2  -\
                                         self.target(x) ** 2,
                              -1, 1)[0]
    def main(self):
        beta = 0
        sqbeta = 0
        for _ in range(self.ntrial):
            res = self.trial()
            beta += res["coef"]
            sqbeta += res["sqcoef"]

        beta /= self.ntrial
        sqbeta /= self.ntrial

        bias = self.calc_bias(beta)
        var = self.calc_variance(sqbeta)
        eout = bias + var

        ret = {}
        ret["beta"] = beta
        ret["bias"] = bias
        ret["var"] = var
        ret["eout"] = eout
        return ret

class ConstBiasVariance(BiasVariance):
    def __init__(self):
        super(ConstBiasVariance, self).__init__()

    @staticmethod
    def fit(D):
        return np.array([D[1].mean()])

    def trial(self):
        D = self.generate_data()
        result = {}
        coef = self.fit(D)
        result["coef"] = coef
        result["sqcoef"] = coef ** 2
        return result

    def calc_bias(self, beta):
        return integrate.quad(lambda x: (beta  - self.target(x)) ** 2,
                              -1, 1)[0]

    def calc_variance(self, moment2):
        return integrate.quad(lambda x: moment2 * -\
                                         self.target(x) ** 2,
                              -1, 1)[0]
    def main(self):
        beta = 0
        sqbeta = 0
        for _ in range(self.ntrial):
            res = self.trial()
            beta += res["coef"]
            sqbeta += res["sqcoef"]

        beta /= self.ntrial
        sqbeta /= self.ntrial

        bias = self.calc_bias(beta)
        var = self.calc_variance(sqbeta)
        eout = bias + var

        ret = {}
        ret["beta"] = beta
        ret["bias"] = bias
        ret["var"] = var
        ret["eout"] = eout
        return ret


class LinearIntBV(BiasVariance):
    def __init__(self):
        super(LinearIntBV, self).__init__()

    @staticmethod
    def fit(D):
        lm = LinearRegression(fit_intercept = True)
        lm.fit(D[0], D[1])
        return lm

    def trial(self):
        D = self.generate_data()
        result = {}
        lm = self.fit(D)
        result["coef"] = lm.coef_
        result["intercept"] = lm.intercept_
        return result

    def calc_bias(self, beta, intercept):
        return integrate.quad(lambda x:\
                              (beta * x + intercept  - self.target(x)) ** 2,
                              -1, 1)[0]

    def calc_variance(self, sq_square, sq_lin, sq_const):
        return integrate.quad(lambda x:\
                              (sq_square * x ** 2 + sq_lin * x + sq_const)  -\
                              self.target(x) ** 2,
                              -1, 1)[0]
    def main(self):
        beta = 0
        intercept = 0

        sq_square = 0
        sq_lin = 0
        sq_const = 0

        for _ in range(self.ntrial):
            res = self.trial()
            coef = res["coef"]
            intercept = res["intercept"]

            beta += coef
            intercept += intercept
            sq_square += coef ** 2
            sq_lin += 2 * coef * intercept
            sq_const += intercept ** 2

        beta /= self.ntrial
        intercept  /= self.ntrial
        sq_square /= self.ntrial
        sq_lin /= self.ntrial
        sq_const /= self.ntrial

        bias = self.calc_bias(beta, intercept)
        var = self.calc_variance(sq_square, sq_lin, sq_const)
        eout = bias + var

        ret = {}
        ret["beta"] = beta
        ret["bias"] = bias
        ret["var"] = var
        ret["eout"] = eout
        return ret

class QuadraticBV(BiasVariance):
    def __init__(self):
        super(QuadraticBV, self).__init__()

    @staticmethod
    def fit(D):
        Dtrans = (D[0] ** 2 , D[1])
        lm = LinearRegression(fit_intercept = False)
        lm.fit(Dtrans[0], Dtrans[1])
        return lm.coef_

    def trial(self):
        D = self.generate_data()
        result = {}
        coef = self.fit(D)
        result["coef"] = coef
        result["sqcoef"] = coef ** 2
        return result

    def calc_bias(self, beta):
        return integrate.quad(lambda x: (beta * x ** 2  - self.target(x)) ** 2,
                              -1, 1)[0]

    def calc_variance(self, moment2):
        return integrate.quad(lambda x: moment2 * x ** 4  -\
                                         self.target(x) ** 2,
                              -1, 1)[0]
    def main(self):
        beta = 0
        sqbeta = 0
        for _ in range(self.ntrial):
            res = self.trial()
            beta += res["coef"]
            sqbeta += res["sqcoef"]

        beta /= self.ntrial
        sqbeta /= self.ntrial

        bias = self.calc_bias(beta)
        var = self.calc_variance(sqbeta)
        eout = bias + var

        ret = {}
        ret["beta"] = beta
        ret["bias"] = bias
        ret["var"] = var
        ret["eout"] = eout
        return ret

class QuadraticIntBV(BiasVariance):
    def __init__(self):
        super(QuadraticIntBV, self).__init__()

    @staticmethod
    def fit(D):
        Dtrans = (D[0] ** 2 , D[1])
        lm = LinearRegression(fit_intercept = True)
        lm.fit(Dtrans[0], Dtrans[1])
        return lm

    def trial(self):
        D = self.generate_data()
        result = {}
        lm = self.fit(D)
        result["coef"] = lm.coef_
        result["intercept"] = lm.intercept_
        return result

    def calc_bias(self, beta, intercept):
        return integrate.quad(lambda x:\
                              (beta * x ** 2 + intercept  -\
                               self.target(x)) ** 2,
                              -1, 1)[0]

    def calc_variance(self, sq_square, sq_lin, sq_const):
        return integrate.\
            quad(lambda x:\
                 (sq_square * x ** 4 + sq_lin * x ** 2 + sq_const)  -\
                 self.target(x) ** 2,
                 -1, 1)[0]

    def main(self):
        beta = 0
        intercept = 0

        sq_square = 0
        sq_lin = 0
        sq_const = 0

        for _ in range(self.ntrial):
            res = self.trial()
            coef = res["coef"]
            intercept = res["intercept"]

            beta += coef
            intercept += intercept
            sq_square += coef ** 2
            sq_lin += 2 * coef * intercept
            sq_const += intercept ** 2

        beta /= self.ntrial
        intercept  /= self.ntrial
        sq_square /= self.ntrial
        sq_lin /= self.ntrial
        sq_const /= self.ntrial

        bias = self.calc_bias(beta, intercept)
        var = self.calc_variance(sq_square, sq_lin, sq_const)
        eout = bias + var

        ret = {}
        ret["beta"] = beta
        ret["bias"] = bias
        ret["var"] = var
        ret["eout"] = eout
        return ret

class FitTester(object):
    def __init__(self):
        self.logic = {
            "linear" : LinearBiasVariance(),
            "const" : ConstBiasVariance(),
            "lin_int" : LinearIntBV(),
            "quadratic" : QuadraticBV(),
            "quadratic_int" : QuadraticIntBV(),
        }

    def get_error(self, name):
        model = self.logic[name]
        return model.main()["eout"]

    def main(self):
        model_names = self.logic.keys()
        eouts = []
        for mn in model_names:
            eout = self.get_error(mn)
            eouts.append(eout)

        min_eout = min(eouts)
        min_idx = eouts.index(min_eout)
        best_model = model_names[min_idx]
        return {"best_model" : best_model,
                "min_eout" : min_eout}

def p456():
    ee = LinearBiasVariance()
    res = ee.main()
    print("p4: g(x) = %.3f x "% res["beta"])
    print("p5: bias = %.2f " % res["bias"])
    print("p6: variance = %.2f " % res["var"])

def p7():
    ft = FitTester()
    res = ft.main()
    print("p7: best model  - %s" % res["best_model"])






def main():
    # print("p1 : %d" % p1())
    # plot_bounds()
    # p456()
    p7()


if __name__ == "__main__":
    main()
