from __future__ import print_function
import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


def error_cat(pred, actual):
    n = len(pred)
    wrong = [(0 if pred[i] == actual[i] else 1)\
            for i in range(n)]
    return float(sum(wrong))/n


def choose_random_coord(prange):
    return random.uniform(*prange)


def choose_random_point(prange):
    return np.array([1, choose_random_coord(prange), choose_random_coord(prange)])


def choose_random_line(prange):
    p1 = choose_random_point(prange)
    p2 = choose_random_point(prange)
    w1 = -1 * (p2[2] - p1[2]) / (p2[1] - p1[1])
    w0 = -1 * w1 * p1[1] - p1[2]
    l = np.array([w0, w1, 1])
    return l

def get_line_eq(v):
    return lambda x: (-1 * v[0] - v[1] * x) / v[2]

def generate_data_set(prange, N, f):
    points = [choose_random_point(prange) for _ in range(N)]
    return [[p, f(p)] for p in points]

class Perceptron(object):
    def __init__(self, N = 100, size = 1, pocket = True, plot = False):
        self.N = N
        self.size = size
        self.prange = [-1 * size, size]
        self.f = None
        self.D = None
        self.plot = plot
        self.nerror_trial = 100
        self.pocket = pocket
        self.g = None

    @classmethod
    def regr_transform(cls, perceptron_D):
        xs = tuple(map(lambda x: x[0], perceptron_D))
        X = np.vstack(xs)
        y = np.array(map(lambda x: x[1], perceptron_D))
        return X, y

    @staticmethod
    def get_pocket_weights(D):
        X, y = Perceptron.regr_transform(D)
        lm = LinearRegression(fit_intercept = False)
        lm.fit(X, y)
        return lm.coef_
    
    @staticmethod
    def get_initial_weights(D):
        if len(D) == 0:
            raise ValueError("Empty Dataset!")
        dim = len(D[0][0])
        return np.array([0 for _ in range(dim)])

    @staticmethod
    def pla(D, weight):
        if len(D) == 0:
            raise ValueError("Empty Dataset!")

        N = len(D)
        misclassified = D
        L = N
        niter = 0

        while L > 0:
            niter += 1
            idx = random.randint(0,L - 1)
            x, y = misclassified[idx]
            weight = weight + y * x
            misclassified = [dd for dd in D if Perceptron.decide(weight, dd[0])!= dd[1]] 
            L = len(misclassified)

        return {"n" : niter, "g" :weight}

    @staticmethod
    def decide(weights, point):
        return np.sign(np.dot(weights, point))

    def generate_data_set(self):
        return generate_data_set(self.prange, self.N, lambda p: self.decide(self.f, p))

    def plot_points(self):
        pts = map(lambda x: x[0], self.D)

        x1 = map(lambda x: x[1], pts)
        x2 = map(lambda x: x[2], pts)

        x_range = np.array(range(-1 * self.size, self.size + 1))

        axes = plt.gca()
        axes.set_xlim(*self.prange)
        axes.set_ylim(*self.prange)

        plt.plot(x_range, get_line_eq(self.f)(x_range), c = "green")
        plt.plot(x_range, get_line_eq(self.g)(x_range), c = "red")
        plt.scatter(x1,x2)
        plt.show()

    def sim_error_rate(self):
        nerrors = 0
        for _ in range(self.nerror_trial):
            pt = choose_random_point(self.prange)
            if self.decide(self.f, pt) != self.decide(self.g, pt):
                nerrors += 1
        return float(nerrors)/ self.nerror_trial


    def main(self):
        self.f = choose_random_line(self.prange)
        self.D = self.generate_data_set()
        weight_func = self.get_pocket_weights if self.pocket else self.get_initial_weights
        weight = weight_func(D)
        result = self.pla(self.D, weight)
        self.g = result["g"]

        if self.plot:
            self.plot_points()

            print("iterations for convergence: %d" % result["n"])
            print("f: %s" % self.f)
            print("g: %s" % result["g"])

        result["Eout"] = self.sim_error_rate()

        return result

class Tester(object):
    def __init__(self, Trial, ntrial, stats = None, verbose = False):
        self.Trial = Trial
        self.ntrial = ntrial
        self.stat_list = stats
        self.verbose = verbose

    def get_stat_list(self):
        ret = None
        if self.stat_list:
            ret = self.stat_list
        else:
            result = self.Trial.main()
            ret = result.keys()
        return ret

    def main(self):
        stat_list = self.get_stat_list()
        agg_results = {}
        for s in stat_list:
            agg_results[s] = 0

        for i in range(self.ntrial):

            if self.verbose:
                if i % 10 == 0:
                    print(i)

            result = self.Trial.main()
            for s in stat_list:
                agg_results[s] += result[s]

        for s in stat_list:
            agg_results[s] = agg_results[s] / float(self.ntrial)

        return agg_results

def dataset_to_xy(D):
    X = np.array([d[0] for d in D])
    y = np.array([d[1] for d in D])
    return X,y 

if __name__ == "__main__":
    main()
