from __future__ import print_function
from math import exp, log10
import numpy as np
import pandas as pd
from ml_lib import choose_random_point, choose_random_line, generate_data_set
import matplotlib.pyplot as plt
from scipy.special import expit
from math import exp, log


class P5Err(object):

    def __init__(self):
        self.eps = 0.001

    @staticmethod
    def err(point):
        u, v = point
        return (u * exp(v) - 2 * v * exp(-1 * u)) ** 2

    @staticmethod
    def aa(point):
        u, v = point
        return u * exp(v) - 2 * v * exp(-1 * u)

    def du(self, point):
        u, v = point
        return 2 * self.aa(point) * (exp(v) + 2 * v * exp(-1 * u))

    def dv(self, point):
        u, v = point
        return 2 * self.aa(point) * (u * exp(v) - 2 * exp(-1 * u))

    def grad(self, point):
        return np.array([self.du(point), self.dv(point)])

    def num_grad(self, point):
        u0 = point[0]
        v0 = point[1]
        approx_du = (self.err(np.array([u0 + self.eps, v0])) -
                     self.err(point))/self.eps
        approx_dv = (self.err(np.array([u0, v0 + self.eps])) -
                     self.err(point))/self.eps
        return np.array([approx_du, approx_dv])


def p56():
    err_calc = P5Err()
    initial = np.array([1, 1])
    eta = 0.1

    err = err_calc.err(initial)
    niter = 0
    x = initial

    while err >= 1e-14:
        grad = err_calc.grad(x)
        x = x + -1 * eta * grad
        err = err_calc.err(x)
        niter += 1
    print("converged to %s in %d iterations" % (x, niter))


def p7():
    err_calc = P5Err()
    initial = np.array([1, 1])
    eta = 0.1
    x = initial
    for _ in range(15):
        du = err_calc.du(x)
        x = x + -1 * eta * np.array([du, 0])
        dv = err_calc.dv(x)
        x = x + -1 * eta * np.array([0, dv])

    err = err_calc.err(x)
    log_err = log10(err)
    print("coordinate descent error: 10 ** %d" % round(log_err))

class P89():
    def __init__(self):
        self.prange = (-1,1)
        self.N = 100
        self.line = choose_random_line(self.prange)

    @staticmethod
    def decide(line,  point):
        return np.sign(np.dot(line, point))

    def f(self, point):
        return self.decide(self.line, point)

    def generate_data(self):
        return generate_data_set(self.prange, self.N, self.f)

    @staticmethod
    def get_line_eq(v):
        return lambda x: (-1 * v[0] - v[1] * x) / v[2]

    def ce_error(self, pt, label, weight):
        return log(1 + exp(-1 * label * np.dot(pt, weight)))

    def calc_error(self, h, D):
        return np.array([self.ce_error(x, y, h) for x, y in D]).mean()

    def gradient(self, pt, label, weight):
        return -1 * (label * pt) / (1 + exp(label * np.dot(weight, pt)))

    def num_grad(self, pt, label, weight):
        eps = 0.001

        def ce_error(x):
            return self.ce_error(x, label, weight)
        b0, x0, y0 = pt
        err0 = ce_error(pt)
        db = (ce_error(np.array([b0 + eps, x0, y0])) - err0) / eps
        dx = (ce_error(np.array([b0, x0 + eps, y0])) - err0) / eps
        dy = (ce_error(np.array([b0, x0, y0 + eps])) - err0) / eps
        return np.array([db, dx, dy])

    def sgd(self, D):
        eta = 0.01
        pts = [x[0] for x in D]
        labels = [x[1] for x in D]
        w0 = np.array([0.0, 0.0, 0.0])
        w_iter = w0[:]
        epoch = 0
        while True:
            order = np.random.permutation(self.N)
            for ord in order:
                label = labels[ord]
                pt = pts[ord]
                gradient = self.gradient(pt, label, w_iter)
                w_iter = w_iter -  eta * gradient
            epoch += 1
            # print("epoch: %d, ein : %.3f, w: %s, w0: %s" % (epoch, self.calc_error(w_iter, D), w_iter, w0))
            if np.linalg.norm(w_iter - w0) < 0.01:
                break
            w0 = w_iter[:]
        return w_iter, epoch

    def plot_data(self, D):
        pts = [x[0] for x in D]
        labels = [x[1] for x in D]
        x1 = [i for _,i,_ in pts]
        x2 = [i for _,_,i in pts]
        lb, ub = self.prange
        x_range = np.array(range(lb, ub + 1))
        axes = plt.gca()
        axes.set_xlim(*self.prange)
        axes.set_ylim(*self.prange)
        colors = ["green" if label == 1 else "red" for label in labels]
        plt.scatter(x1, x2, c=colors)
        plt.plot(x_range, self.get_line_eq(self.line)(x_range), c="green")
        plt.show()


    def main(self):
        D = self.generate_data()
        g, epochs = self.sgd(D)
        eout = self.calc_error(g, self.generate_data())
        return eout, epochs

def p89():
    res = [P89().main() for _ in range(100)]
    df = pd.DataFrame(res)
    df.columns = "eout nepoch".split()
    eout_se = df.eout.std() / df.eout.count()
    epoch_se = df.nepoch.std() / df.nepoch.count()
    print("err : (%.4f, %.4f), epochs: (%d, %d)" % (df.eout.mean(), eout_se, df.nepoch.mean(), epoch_se))
    import IPython; IPython.embed()

    # print("mean err : %.5f" % errs.mean())

def main():
    # p56()
    # p7()
    p89()


if __name__ == "__main__":
    main()
